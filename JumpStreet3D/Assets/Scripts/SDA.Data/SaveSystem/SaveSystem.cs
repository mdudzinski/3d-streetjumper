using System;
using UnityEngine;
using System.IO;

namespace SDA.Data
{

    [Serializable]
    public class PlayerData
    {
        public int lastScore;
        public int bestScore;

        public PlayerData()
        {
            lastScore = 0;
            bestScore = 30;
        }
    }

    public class SaveSystem
    {
        public const string SAVE_NAME = "data.json";
        public PlayerData PlayerData { get; set; }


        public void LoadData()
        {
            var savePath = Path.Combine(Application.persistentDataPath, SAVE_NAME);
            Debug.Log(savePath);
            if (File.Exists(savePath))
            {
                var rawFile = File.ReadAllText(savePath); //zwraca tekst z pliku w jednym długim stringu
                PlayerData = JsonUtility.FromJson<PlayerData>(rawFile); //JSON UTILITY - specjalna biblioteka, która pozwala z pliku zrobić JSON'a albo zapisać go jako JSON
            }
            else
            {
                PlayerData = new PlayerData();
                SaveData();
            }
        }

        public void SaveData()
        {
            var savePath = Path.Combine(Application.persistentDataPath, SAVE_NAME);
            var rawFile = JsonUtility.ToJson(PlayerData, true); //prettyPrint opcja, która formatuje tekst pliku w "ładny, poukładany" sposób (jeżeli napiszemy true, jeżeli napiszemy false - wrzuci tekst, bez odstępów, formatowania, w jednej linijce)
            File.WriteAllText(savePath, rawFile);
        }

    }
}
