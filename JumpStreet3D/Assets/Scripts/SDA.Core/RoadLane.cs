using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadLane : BaseLane, ISpawnable
{

    [SerializeField]
private GameObject[] whiteRectangles;

    public override void PrepareForActivate()
    {
        foreach (var rect in whiteRectangles)
            rect.SetActive(false);
    }

    public override void RefreshObjectState()
    {
        foreach (var rect in whiteRectangles)
            rect.SetActive(enableAdditionalObjects);
    }

}
