using System.Collections;
using System;
using UnityEngine;

public enum LaneType
{
    Green,
    Road,
    Track
}

public enum LaneDirection
{
    Left,
    Right
}

[Serializable]
public class LaneData
{
    public LaneType type;
    public LaneDirection dir;
    public bool enableAdditionalObjects;
}

[CreateAssetMenu(menuName = "Bundle/new bundle")]
public class LanesBundle : ScriptableObject
{
    public LaneData[] lanes;
}
