using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random; //specjalny using, który określa której klasy "Radnom" chcemy użyć, czy tej zawartej w usingu UnityEngine czy w usingu System

public class NormalVehicle : BaseVehicle
{
    [SerializeField]
    private float vehicleSpeed = 50.0f;

    [SerializeField]
    private Rigidbody vehicleRb;

    public void StartMovement()
    {
        vehicleRb.AddForce(this.transform.forward * vehicleSpeed, ForceMode.Impulse);
    }

    public void AddListener(Action<NormalVehicle> callback)
    {
        Action<NormalVehicle> onDestroyCallback = null;
        onDestroyCallback += callback;
    }

    void Start()
    {
        
    }

    void Update()

    {
        transform.Translate(Vector3.forward * vehicleSpeed * Time.deltaTime);
    }
}
