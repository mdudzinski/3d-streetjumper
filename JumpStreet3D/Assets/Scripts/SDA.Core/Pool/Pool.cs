using System.Collections.Generic;
using UnityEngine;

public class Pool<TItem> : MonoBehaviour
    where TItem : MonoBehaviour, IPoolable //"gdzie TItem (ten obiekt) ma dziedziczyć po MonoBehavior i mieć zaimplementowany interface IPoolable"
{
    private int size; //zmienna mówiąca ile maksymalnie przedmiotów POOL może przechować, jak dóży jest POOL

    //private int count => internalCollection.Count; //zmienna mówiąca ile aktualnie przemiotów znajduje się w POOLu

    [SerializeField]
    private TItem originalPrefab;
    private Stack<TItem> storedItem; //stos wygenerowanych obiektów



    public void Initialize(int size) //metoda mówica ile przedmiotów POOL może przechowywać
    {
        storedItem = new Stack<TItem>(size);
        for (var i = 0; i < size; ++i)
        {
            var newObj = SpawnNewObject();

            storedItem.Push(newObj); //Push -> dodaje do kolekcji/stosu; Pop -> zwraca pierwszy obiekt z góry i usuwa z kolekcji/stosu
        }
        this.size = size;
    }

    public TItem GetFromPool(Vector3 position) //metoda, która zwraca z POOL'a
    {

        TItem toReturn = storedItem.Count == 0 ? SpawnNewObject() : storedItem.Pop();

        toReturn.transform.position = position;
        toReturn.transform.rotation = originalPrefab.transform.rotation;

        toReturn.transform.SetParent(null);
        toReturn.PrepareForActivate();
        toReturn.gameObject.SetActive(true);

        return toReturn;
    }

    //public TItem GetFromPool(Vector3 position, Quaternion rotation) //metoda, która zwraca z POOL'a
    //{
    //    TItem toReturn = GetFromPool(position);
    //    toReturn.transform.rotation = rotation;
    //    return toReturn;
    //}


    public void ReturnToPool(TItem item)
    {
        if (storedItem.Count < size)
        {
            item.PrepareForDeactivate();
            item.gameObject.SetActive(false);
            item.transform.SetParent(this.transform);
            storedItem.Push(item);
        }
        else
        {
            Destroy(item.gameObject);
        }
    }

    private TItem SpawnNewObject()
    {
        var newObj = Instantiate(originalPrefab, Vector3.zero, Quaternion.identity, this.transform);
        newObj.gameObject.SetActive(false);
        return newObj;
    }
}