public interface IPoolable // interface zawierajacy metody, które chcemy, żeby każdy obiekt wykonywał
{
    void PrepareForActivate();

    void PrepareForDeactivate();
}
