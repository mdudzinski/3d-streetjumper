using System;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class BaseLane : MonoBehaviour, IPoolable
{
    //te metody implementujemy tylko w BaseLane bo pozostałe "Lane'y" dziedziczą po BaseLane więc nie musimy implementować ich w każdym pojedyńczym Lane, za to robimy tam override

    [SerializeField]
    private Vector2 timeFrame;

    [SerializeField]
    private Transform[] spawnPoints;

    [SerializeField]
    private MeshRenderer meshRenderer;

    [FormerlySerializedAs("lightGreen")]
    [SerializeField]
    private Color baseColor;

    [FormerlySerializedAs("darkGreen")]
    [SerializeField]
    private Color alternativeColor;

    private Action<BaseLane> onDespawn;

    private LaneDirection direction;
    protected bool enableAdditionalObjects;

    public void SetAdditionalObjectState(bool newState)
    {
        enableAdditionalObjects = newState;
    }

    public virtual void RefreshObjectState()
    {

    }

    public void AddListener(Action<BaseLane> callback)
    {
        onDespawn += callback; //przekazuje metodę, którą dodajemy do onDespawn, i jak zrobimy invoke, to on wywoła wszystko z onDespawn
    }

    public void SetDirection(LaneDirection laneDirection)
    {
        this.direction = laneDirection;
    }

    public Vector3 GetSpawnPointPosition()
    {
        return spawnPoints[(int)direction].position;
    }

    public Quaternion GetSpawnPointRotation()
    {
        return spawnPoints[(int)direction].rotation;
    }

    public Vector2 GetTimeFrame()
    {
        return timeFrame;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Despawner"))
        {
            onDespawn?.Invoke(this); //"this" przekazuje obiekt, do którego podpięty jest ten skrypt / "obiekt?.Metoda()" -> sprawdza czy obiekt jest różny od null, czy coś w nim jest;
        }
    }

    public void SetColor(int count)
    {
        meshRenderer.material.color = count % 2 == 0 ? baseColor : alternativeColor;
    }

    public virtual void PrepareForActivate()
    {
        
    }

    public virtual void PrepareForDeactivate()
    {
        
    }
}