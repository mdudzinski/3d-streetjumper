using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSpawner : MonoBehaviour
{
    [SerializeField]
    private NormalVehiclePool normalVehiclePool;

    private List<ISpawnable> lanesInGame = new List<ISpawnable>();

    private Dictionary<ISpawnable, Coroutine> lanesToCoroutineDic = new Dictionary<ISpawnable, Coroutine>();

    public void InitSpawner()
    {
        normalVehiclePool.Initialize(20);   
    }

    private IEnumerator SpawnVehicleOnLane(ISpawnable lane)
    {
        while (true)
        {
            var vehicle = normalVehiclePool.GetFromPool(lane.GetSpawnPointPosition());
            vehicle.AddListener(DespawnVehicle);
            vehicle.transform.rotation = lane.GetSpawnPointRotation();

            var timeFrame = lane.GetTimeFrame();
            var timeToNextSpawn = Random.Range(timeFrame.x, timeFrame.y);
            yield return new WaitForSeconds(timeToNextSpawn);
        }
    }

    private void DespawnVehicle(BaseVehicle vehicle)
    {
        normalVehiclePool.ReturnToPool(vehicle as NormalVehicle);
    }


    public void Subscribe(ISpawnable item)
    {
        if (!lanesInGame.Contains(item)) //Contains -> sprawdza czy item jest w liście
            lanesInGame.Add(item);
        if(!lanesToCoroutineDic.ContainsKey(item))
        {
            var coroutine = StartCoroutine(SpawnVehicleOnLane(item));
            lanesToCoroutineDic.Add(item, coroutine);
        }
    }

    public void Unsubscribe(ISpawnable item)
    {
        if (lanesInGame.Contains(item))
        {
            if (lanesToCoroutineDic.ContainsKey(item))
            {
                StopCoroutine(lanesToCoroutineDic[item]);
                lanesToCoroutineDic.Remove(item);
            }
            lanesInGame.Remove(item);
        }
    }
}
