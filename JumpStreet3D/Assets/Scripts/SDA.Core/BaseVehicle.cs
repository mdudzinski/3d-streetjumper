using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BaseVehicle : MonoBehaviour, IPoolable
{

    private Action<BaseVehicle> onDespawn;

    public virtual void PrepareForActivate()
    {
        
    }

    public virtual void PrepareForDeactivate()
    {
        onDespawn = null;
    }

    public void AddListener(Action<BaseVehicle> callback) 
    {
        onDespawn += callback; //przekazuje metodę, którą dodajemy do onDespawn, i jak zrobimy invoke, to on wywoła wszystko z onDespawn
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Despawner"))
        {
            onDespawn?.Invoke(this); //"this" przekazuje obiekt, do którego podpięty jest ten skrypt / "obiekt?.Metoda()" -> sprawdza czy obiekt jest różny od null, czy coś w nim jest;
        }
    }
}
