
using SDA.Data;
using SDA.UI;
using UnityEngine;

namespace SDA.Architecture
{
    public class GameController : MonoBehaviour
    {

        private BaseState currentState;//zmienna, która trzyma
                                       //aktualnie uruchomiony stan

        private SaveSystem saveSystem;
        public SaveSystem SaveSystem => saveSystem;

        [SerializeField]
        private CrossyInput crossyInput;
        public CrossyInput CrossyInput => crossyInput;

        [SerializeField]
        private MenuView menuView;
        public MenuView MenuView => menuView;

        [SerializeField]
        private LevelSpawner levelSpawner;
        public LevelSpawner LevelSpawner => levelSpawner;

        [SerializeField]
        private VehicleSpawner vehicleSpawner;
        public VehicleSpawner VehicleSpawner => vehicleSpawner;

        [SerializeField]
        private CameraMovementController cameraMovementController;
        public CameraMovementController CameraMovementController => cameraMovementController;

        [SerializeField]
        private PlayerMovement playerMovement;
        public PlayerMovement PlayerMovement => playerMovement;



        void Start()
        {
            saveSystem = new SaveSystem();
            saveSystem.LoadData();

            // inicjalizacja zmiennych
            //ustawienia startowego stanu
            ChangeState(new MenuState());
        }

        void Update()
        {
            //if (currentState != null)
            //    currentState.Update();

            currentState?.UpdateState();//to jest inna forma
                                        //zapisu tego co powyżej
        }

        private void OnDestroy()
        {
            saveSystem.SaveData();

            //co ma się stać przed wyłączniem gry -> np save
            ChangeState(null);
        }

        public void ChangeState(BaseState newState)
        {
            currentState?.DestroyState();
            currentState = newState;
            currentState?.InitState(this); //this -> obiekt na którym,
                                           //znajduje się ten skrypt
        }
    }
}
