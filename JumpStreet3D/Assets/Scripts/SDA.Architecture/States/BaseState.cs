using UnityEngine;

namespace SDA.Architecture
{

    public abstract class BaseState //abstrakcyjna klasa z zaimplementowanymi metodami
                                    //do stworzenia zainicjowania MachineState
    {
        protected GameController controller;

        public virtual void InitState(GameController controller)
        {
            this.controller = controller;
        }

        public virtual void UpdateState()
        {

        }


        public virtual void DestroyState()
        {
        }
    }
}

