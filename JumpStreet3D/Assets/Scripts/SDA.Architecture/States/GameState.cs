using UnityEngine;

namespace SDA.Architecture
{

    public class GameState : BaseState
    {
        public override void InitState(GameController controller)
        {
            base.InitState(controller);
            controller.VehicleSpawner.InitSpawner();
            controller.LevelSpawner.Init(2);
            controller.CrossyInput.IsMovePressed += controller.PlayerMovement.UpdateMovement;
        }

        public override void UpdateState()
        {
            controller.CameraMovementController.UpdateCameraPosition();
        }

        public override void DestroyState()
        {
            controller.CrossyInput.ClearAllInputs();
        }

    }
}