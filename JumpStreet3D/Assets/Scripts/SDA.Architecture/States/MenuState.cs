using UnityEngine;

namespace SDA.Architecture
{

    public class MenuState : BaseState
    {

        public override void InitState(GameController controller)
        {
            base.InitState(controller);
            controller.MenuView.ShowView();
            controller.CrossyInput.IsStartPressed += StartGame;
        }

        public override void UpdateState()
        {
            Debug.Log("MENU STATE");
        }

        public override void DestroyState()
        {
            controller.MenuView.HideView();
            controller.CrossyInput.ClearAllInputs();
        }

        private void StartGame (bool isPressed)
        {
            if (isPressed)
                controller.ChangeState(new GameState());
        }
    }
}