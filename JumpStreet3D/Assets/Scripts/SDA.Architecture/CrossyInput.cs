using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class CrossyInput : MonoBehaviour
{

    public Action<bool> IsStartPressed;
    public Action<bool> IsPausePressed;
    public Action<Vector2> IsMovePressed;


    public void StartGame(InputAction.CallbackContext ctx)
    {
        if (ctx.performed || ctx.canceled)
        {
            var val = ctx.ReadValueAsButton();
            IsStartPressed?.Invoke(val);
        }
    }

    public void PauseGame(InputAction.CallbackContext ctx)
    {
        if (ctx.performed || ctx.canceled)
        {
            var val = ctx.ReadValueAsButton();
            IsPausePressed?.Invoke(val);
        }
    }

    public void Move(InputAction.CallbackContext ctx)
    {
        if (ctx.performed || ctx.canceled)
        {
            var val = ctx.ReadValue<Vector2>();
            IsMovePressed?.Invoke(val);
        }
    }

    public void ClearAllInputs()
    {
        IsStartPressed = null;
        IsPausePressed = null;
        IsMovePressed = null;
    }

}
