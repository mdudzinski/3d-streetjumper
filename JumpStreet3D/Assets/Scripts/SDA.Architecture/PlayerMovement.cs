using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private Rigidbody rigid;

    private float MoveDirection;


    void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            MoveDirection = -1.0f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            MoveDirection = 1.0f;
        }
        else
        {
            MoveDirection = 0.0f;
        }
    }
}
